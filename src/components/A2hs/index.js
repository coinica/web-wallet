import React from 'react'
import AddToHomeScreen from '@ideasio/add-to-homescreen-react';

export default function A2hs() {
  return (
    <div>
      <AddToHomeScreen
        // appId='Add-to-Homescreen React Live Demo'
        startAutomatically={ true }
        startDelay={ 0 }
        lifespan={ 3000 }
        skipFirstVisit={ false }
        displayPace={ 0 }
        customPromptContent={ {
          cancelMsg: '',
          installMsg: 'Install',
          guidanceCancelMsg: ''
        } }
      />
    </div>
  )
}
