import React, { Component } from "react";
import { Avatar, Dropdown } from "antd";
import {
  UserOutlined,
} from "@ant-design/icons";
import { connect } from "react-redux";
import { setWalletKeys, setAuth, setEmail, setUpgrading } from "../../redux/actions";
import { SettingsMenu } from '../../components'
import "./style.scss";
// import coinicaLogo from "../../assets/images/coinica_logo.png";

function mapStateToProps(state) {
  return {
    email: state.auth.email,
    securityLevel: state.auth.securityLevel,
  };
}

const mapDispatchToProps = (dispatch) => ({
  setWalletKeys(arg) {
    dispatch(setWalletKeys(arg));
  },
  setAuth(arg) {
    dispatch(setAuth(arg));
  },
  setUpgrading(arg) {
    dispatch(setUpgrading(arg));
  },
});

class TopHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: null,
    };
  }

  render() {
    const { email } = this.props;

    const menu = <SettingsMenu />;

    return (
      <div className="top-header">
        <span className="avatar-container">
          <Dropdown overlay={menu} placement="bottomRight">
            <span>
              <Avatar icon={<UserOutlined />} />
              <span className="avatar-email-container">{email}</span>
            </span>
          </Dropdown>
        </span>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TopHeader);
