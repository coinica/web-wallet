import React, { Component } from 'react'
import { connect } from 'react-redux'
import Assets from '../../../assets/assets.json'
import './style.scss'

const coingeckoApi = "https://api.coingecko.com/api/v3";

export const getCoinData = (coin) => {
  return fetch(`${coingeckoApi}/coins/${coin}`)
    .then(response => response.json());
};

export class WalletAssetsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getCoingeckoInfo();
  }
  
  getCoingeckoInfo = () => {
    const { indexArray } = this.props;
    for (const index of indexArray) {
      console.log('index: \n', index);
      const coinId = Assets[index].id;
      const pointer = this;

      getCoinData(coinId).then(res => {
        console.log('coingecko: \n', coinId, res);
        pointer.setState({
          [coinId]: {
            price: res.market_data.current_price.usd,
            priceChange: res.market_data.price_change_24h / res.market_data.current_price.usd * 100
          }
        })

        if (window.coingeckoTimer) {
          clearInterval(window.coingeckoTimer)
        }

        window.coingeckoTimer = setInterval(pointer.getCoingeckoInfo, 3000);
      }).catch(e => console.log(e))
    }
  }

  onAssetClick = (index) => () => {
    this.getCoingeckoInfo();
    this.props.onAssetClick(index)();
  }

  getAmount = (index) => {
    try {
      console.log('getAmount: \n', index)
      const { walletKeys } = this.props
      if(index === 'liberdus') {
        let amount = 0;
        for(const key of Object.keys(walletKeys[index].asset.amount)) {
          console.log('key: ', key)
          amount += walletKeys[index].asset.amount[key]
          console.log(amount)
        }
        console.log(amount)
        return amount;
      }
  
      return walletKeys[index].asset.amount
      ? walletKeys[index].asset.amount
      : 0
    } catch(e) {
      console.log(e)
      return 0;
    }
  }

  render() {
    const { walletKeys, indexArray } = this.props
    console.log('WalletAssetSContainer render: \n', this.props)

    return (
      <div>
        {indexArray.map((index) => {
          return (
            <div
              className='wallet-asset-container'
              onClick={this.onAssetClick(index)}
              key={index}
            >
              <div style={{ display: 'flex' }}>
                {Assets[index].icon && (
                  <img src={Assets[index].icon} alt='icon' />
                )}
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                  <span className="asset-name">
                    {Assets[index].name}
                  </span>

                  <span className="asset-value">
                    ${this.state[index] && this.state[index].price ? this.state[index].price.toFixed(2) : '0.00'}
                    {' '}
                    {this.state[index] && this.state[index].priceChange ?
                     this.state[index].priceChange > 0 ?
                     <span style={{color: "green"}}>{this.state[index].priceChange.toFixed(2)}</span> :
                     <span style={{color: "red"}}>{this.state[index].priceChange.toFixed(2)}</span> 
                     : '0.00'}%
                  </span>
                </div>
              </div>
              
              <div className="asset-amount">
                {this.getAmount(index)}{' '}
                {Assets[index].symbol}
              </div>
            </div>
          );
        })}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  walletKeys: state.wallet.keys,
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(WalletAssetsContainer)
