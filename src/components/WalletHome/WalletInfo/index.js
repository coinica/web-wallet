import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import { version } from '../../../utils'

export class WalletInfo extends Component {
  render() {
    return (
      <div className='wallet-info-container'>
        <Row justify='center' className='wallet-balance'>
          $0.00
        </Row>

        <Row justify='center' className='wallet-title'>
          Coinica Wallet {version}
        </Row>

        <Row justify='center' className='wallet-action'>
          <Button
            shape='circle'
            type='primary'
            size='large'
            icon={<PlusOutlined />}
            onClick={this.props.showModal}
          ></Button>
        </Row>

        <Row justify='center' className='wallet-action-name'>
          Add Asset
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(WalletInfo)
