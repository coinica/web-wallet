import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Menu } from 'antd'
import {
  LogoutOutlined,
  SafetyCertificateOutlined,
} from "@ant-design/icons";
import { setWalletKeys, setAuth, setUpgrading } from "../../redux/actions";

export class SettingsMenu extends Component {
  initialize() {
    this.props.setWalletKeys({});
    this.props.setAuth(false);
  }

  onSignOut = () => {
    this.initialize();
  };

  onUpgradeSecurityLevel = () => {
    this.props.setUpgrading(true);
  };

  render() {
    const { securityLevel } = this.props;

    return (
      <Menu>
        {securityLevel === 1 ? (
          <Menu.Item onClick={this.onUpgradeSecurityLevel}>
            <span>
              <SafetyCertificateOutlined />
              Upgrade security level
            </span>
          </Menu.Item>
        ) : (
          <Menu.Item onClick={this.onSignOut}>
            <span>
              <LogoutOutlined />
              Sign Out
            </span>
          </Menu.Item>
        )}
      </Menu>
    )
  }
}

function mapStateToProps(state) {
  return {
    email: state.auth.email,
    securityLevel: state.auth.securityLevel,
  };
}

const mapDispatchToProps = (dispatch) => ({
  setWalletKeys(arg) {
    dispatch(setWalletKeys(arg));
  },
  setAuth(arg) {
    dispatch(setAuth(arg));
  },
  setUpgrading(arg) {
    dispatch(setUpgrading(arg));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsMenu)
