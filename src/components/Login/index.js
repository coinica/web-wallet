import React, { Component } from "react";
import { connect } from "react-redux";
import { Input, Card, Row, Col, Button, Alert } from "antd";
import { KeyOutlined } from "@ant-design/icons";
import { hashKeyFile, upgradeKeyFile } from "../../utils/crypto";
import { setWalletKeys } from "../../redux/actions/wallet";
import {
  setAuth,
  setPassword,
  setUpgrading,
  setSecurityLevel,
} from "../../redux/actions/auth";
import coinicaLogo from "../../assets/images/coinica_logo.png";
import { version } from '../../utils'
import "./style.scss";

export class Login extends Component {
  state = {
    password: "",
    passwordConfirm: "",
    errorMessage: "",
  };

  _inputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onLogInClick = () => {
    const { password, passwordConfirm } = this.state;
    const { upgrading } = this.props;

    if (password.length < 10) {
      this.setState({
        errorMessage: "Password should be longer than 10 letters.",
      });
      return;
    }

    if (upgrading && password !== passwordConfirm) {
      this.setState({
        errorMessage: "Password should match to the confirm password.",
      });
      return;
    }

    this.setState({
      errorMessage: "",
    });

    if (!upgrading) {
      let res = hashKeyFile(2, "", password);

      if (!res.success) {
        this.setState({
          errorMessage: res.message,
        });
        return;
      }

      let keyFile = res.keyFile;

      this.props.setAuth(true);
      this.props.setPassword(password);
      this.props.setWalletKeys(keyFile);
    } else {
      upgradeKeyFile(this.props.walletKeys, password);
      this.props.setAuth(true);
      this.props.setPassword(password);
      this.props.setUpgrading(false);
      this.props.setSecurityLevel(2);
    }
  };

  onCancelUpgrading = () => {
    this.props.setUpgrading(false);
  };

  render() {
    const { upgrading } = this.props;

    const titleContainer = upgrading ? (
      <div className="title-container">
        <span>Upgrade your account</span>
      </div>
    ) : (
      <div className="title-container">
        <span className="login-app-logo">
          <img src={coinicaLogo} alt="logo" />
        </span>
        <span className="app-name">Coinica {version}</span>
      </div>
    );

    const extraContainer = upgrading ? (
      <Button danger type="primary" onClick={this.onCancelUpgrading}>
        Cancel
      </Button>
    ) : null;
    const { password, passwordConfirm, errorMessage } = this.state;

    return (
      <div className='login-container'>
        <Row justify="center">
          <Col xs={22} sm={22} md={20} lg={18} xl={15} xxl={10}>
            <Card title={titleContainer} extra={extraContainer}>
              <Row gutter={[0, 20]}>
                <Col span={24}>
                  <Input.Password
                    size='large'
                    placeholder='Input password'
                    name='password'
                    prefix={<KeyOutlined />}
                    value={password}
                    onChange={this._inputChange}
                  />
                </Col>

                {
                  upgrading &&
                  <Col span={24}>
                    <Input.Password
                      size='large'
                      placeholder='Input password again'
                      name='passwordConfirm'
                      prefix={<KeyOutlined />}
                      value={passwordConfirm}
                      onChange={this._inputChange}
                    />
                  </Col>
                }

                {errorMessage && errorMessage.length > 0 ? (
                  <Col span={24}>
                    <Alert
                      message={errorMessage}
                      type='warning'
                      showIcon
                      closable
                    />
                  </Col>
                ) : (
                  <span />
                )}

                <Col span={24}>
                  <Button
                    size='large'
                    type='primary'
                    shape='round'
                    onClick={this.onLogInClick}
                    className='login-button'
                  >
                    {upgrading ? 'Upgrade' : 'Log In'}
                  </Button>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  walletKeys: state.wallet.keys,
  upgrading: state.auth.upgrading,
});

const mapDispatchToProps = (dispatch) => ({
  setWalletKeys(arg) {
    dispatch(setWalletKeys(arg));
  },
  setAuth(arg) {
    dispatch(setAuth(arg));
  },
  setPassword(arg) {
    dispatch(setPassword(arg));
  },
  setUpgrading(arg) {
    dispatch(setUpgrading(arg));
  },
  setSecurityLevel(arg) {
    dispatch(setSecurityLevel(arg));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
