import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Layout, Dropdown } from 'antd'
import { HomeFilled, SettingFilled } from '@ant-design/icons'
import { SettingsMenu } from '../index'
import { setUpgrading } from "../../redux/actions";
import './style.scss'

const { Footer } = Layout

export class BottomFooter extends Component {
  onHomeClick = () => {
    this.props.setUpgrading(false);
  }

  render() {
    const { isMobile } = this.props;
    const menu = <SettingsMenu />

    return (
      <div>
        {
          !isMobile ?
          <Footer style={{ textAlign: 'center' }}>
            Coinica @2020 Created by Shardus team
          </Footer> :
          <Footer className="mobile-bottom-menu">
            <Row className="mobile-bottom-menu-row">
              <Col span={12} onClick={this.onHomeClick}>
                <Row justify="center" className="icon-row">
                  <HomeFilled />
                </Row>
                <Row justify="center" className="title-row">
                  Home
                </Row>
              </Col>


              <Dropdown overlay={menu} placement="bottomRight">
                <Col span={12}>
                  <Row justify="center" className="icon-row">
                    <SettingFilled />
                  </Row>
                  <Row justify="center" className="title-row">
                    Settings
                  </Row>
                </Col>
              </Dropdown>
            </Row>
          </Footer>
        }    
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  isMobile: state.common.isMobile
})

const mapDispatchToProps = (dispatch) => ({
  setUpgrading(arg) {
    dispatch(setUpgrading(arg));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(BottomFooter)
