/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Button, Select, Row, Col } from 'antd';
import { setWalletKeys } from '../../redux/actions/wallet';
import {
  updateWalletKeys,
  encrypt,
  getRandomMasterKey,
  hashKeyFile
} from '../../utils';
import { setCookie, getSecurityLevel } from '../../utils/cookie';
import {
  setAuth,
  setPassword,
  setSecurityLevel,
} from '../../redux/actions/auth';
import './style.scss';
import Assets from '../../assets/assets.json';
import { WalletInfo, WalletAssetsContainer } from '../../components'

const { Option } = Select;

export class WalletHome extends Component {
  constructor(props) {
    super(props);
    this.visible = React.createRef();
    this.state = {
      visible: false,
      selected_wallet: null,
    };
  }

  onChange = (value) => {
    console.log(`selected ${value}`);
    this.setState({ selected_wallet: value });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    const { selected_wallet } = this.state;
    this.handleCancel();
    const level = getSecurityLevel();
    let res;
    if (level === 1) {
      res = hashKeyFile(level, selected_wallet);
    } else if (level === 2) {
      console.log(selected_wallet);
      console.log(this.props.password);
      res = hashKeyFile(level, selected_wallet, this.props.password);
    }
    this.props.setWalletKeys(res.keyFile);
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  checkChild = (walletKeys, key, password) => () => {
    const keyFile = updateWalletKeys(walletKeys, key, password, getSecurityLevel());
    this.props.setWalletKeys(keyFile)
    if (window.child.closed) {
      console.log('close');
      clearInterval(window.timer);
    }
  };

  onAssetClick = (key) => () => {
    try {
      let { walletKeys } = this.props;
      const itemName = `coinica-${key}`;

      walletKeys = updateWalletKeys(walletKeys, key, this.props.password, getSecurityLevel());
      this.props.setWalletKeys(walletKeys);

      const hashKey = getRandomMasterKey();
      localStorage.setItem(
        itemName,
        JSON.stringify(encrypt(JSON.stringify(walletKeys[key] || {}), hashKey))
      );

      setCookie(itemName, hashKey, '/coinica');
      setCookie(itemName, hashKey, Assets[key].path);
      console.log(Assets[key].path);

      window.child = window.open(Assets[key].path, '_blank');
      window.timer = setInterval(
        this.checkChild(walletKeys, key, this.props.password),
        3000
      );
    } catch (e) {
      console.log(e);
    }
  };

  onSearch = (value) => console.log(value);

  render() {
    console.log('WaleltHome render: \n', this.props.walletKeys);
    try {
      const { walletKeys, isMobile } = this.props;

      const indexArray = Object.keys(walletKeys);

      return (
        <div className='wallet-home-container'>
          <WalletInfo showModal={this.showModal} />
          <WalletAssetsContainer indexArray={indexArray} onAssetClick={this.onAssetClick} />

          <Modal
            centered
            visible={this.state.visible}
            title='Choose wallet'
            onCancel={this.handleCancel}
            onOk={this.handleOk}
            footer={[
              <Button key='back' onClick={this.handleCancel}>
                Cancel
              </Button>,
              <Button key='submit' type='primary' onClick={this.handleOk}>
                Select
              </Button>,
            ]}
          >
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder='Search or select the wallet'
              onChange={this.onChange}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {Object.keys(Assets).map((asset, index) => {
                if (!indexArray.includes(asset)) {
                  return (
                    <Option key={index} value={asset}>
                      {Assets[asset].name}
                    </Option>
                  );
                }
                return null;
              })}
            </Select>
          </Modal>
        </div>
      );
    } catch (e) {
      return <div></div>;
    }
  }
}

const mapStateToProps = (state) => ({
  walletKeys: state.wallet.keys,
  password: state.auth.password,
  auth: state.auth.auth,
  upgrading: state.auth.upgrading,
  securityLevel: state.auth.securityLevel,
  isMobile: state.common.isMobile,
});

const mapDispatchToProps = (dispatch) => ({
  setWalletKeys(arg) {
    dispatch(setWalletKeys(arg));
  },
  setPassword(arg) {
    dispatch(setPassword(arg));
  },
  setAuth(arg) {
    dispatch(setAuth(arg));
  },
  setSecurityLevel(arg) {
    dispatch(setSecurityLevel(arg));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(WalletHome);
