import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setAuth, setPassword, setSecurityLevel, setWalletKeys } from '../../redux/actions';

export class RedirectApp extends Component {
  constructor(props) {
    super(props);
    window.location.href = '/coinica'
  }

  render() {
    return (
      <div />
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth.auth,
    upgrading: state.auth.upgrading,
    securityLevel: state.auth.securityLevel
  };
}

const mapDispatchToProps = (dispatch) => ({
  setWalletKeys(arg) {
    dispatch(setWalletKeys(arg));
  },
  setAuth(arg) {
    dispatch(setAuth(arg));
  },
  setPassword(arg) {
    dispatch(setPassword(arg));
  },
  setSecurityLevel(arg) {
    dispatch(setSecurityLevel(arg));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(RedirectApp)
