export function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

export function setCookie(name, value, path = "/") {
  document.cookie = `${name}=${value}; path=${path}`;
}

export function deleteCookie(name, path = "/") {
  document.cookie = `${name}=; path=${path}`;
}

export function getSecurityLevel() {
  let level = localStorage.getItem("SECURITY_LEVEL");
  console.log("level1: ", level);
  if (level) {
    level = Number(level);
    console.log("level2: ", level);

    if (level === 2) {
      let dKeyHash = getCookie("D_KEY_HASH");
      if (dKeyHash && dKeyHash.length) {
        level = 2;
        localStorage.setItem("SECURITY_LEVEL", 2);
      } else {
        level = 1;
        localStorage.setItem("SECURITY_LEVEL", 1);
      }
    }
  } else {
    level = 1;
    console.log("level3: ", level);
    localStorage.setItem("SECURITY_LEVEL", 1);
  }

  return level;
}
