
function isRunningStandalone() {
  return (window.matchMedia('(display-mode: standalone)').matches);
}

export const getStandaloneInfo = (platform) => {
  let isStandalone = true;
  let useStandaloneMsg =
      'Please add shortcut to the Coinica website on your homescreen and open it to use Coinica wallet. \n Use Safari if you are using iphone.';
  let shouldUpdateMsg = false;

  try {
    const osStr = platform.os.toString()
    if (osStr.includes('iOS') || osStr.includes('Android')) {
      if (isRunningStandalone()) {
        isStandalone = true;
      } else {
        isStandalone = false;

        if (osStr.includes('iOS')) {
          if (platform.name.includes('Safari')) {
            useStandaloneMsg = 'Please add shortcut for Coinica website to your homescreen.'
            shouldUpdateMsg = true
          } else {
            useStandaloneMsg = 'Open Safari and create shortcut for Coinica website.'
          }
        } else {
          useStandaloneMsg = 'Please add shortcut for Coinica website to your homescreen.'
          shouldUpdateMsg = true
        }
      }
    }
  } catch(e) {
    console.log(e)
  }

  return {
    isStandalone,
    useStandaloneMsg,
    shouldUpdateMsg
  }
}