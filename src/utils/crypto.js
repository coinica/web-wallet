import { deleteCookie, getCookie, setCookie } from "./cookie";
// import Assets from "../assets/assets.json";

const crypto = require("crypto");

const algorithm = "aes-256-ctr";
const iv = crypto.randomBytes(16);

export const encrypt = (text, key) => {
  key = Buffer.from(key, "hex");
  const cipher = crypto.createCipheriv(algorithm, Buffer.from(key, "hex"), iv);

  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

  return {
    iv: iv.toString("hex"),
    content: encrypted.toString("hex"),
  };
};

export const decrypt = (hash, key) => {
  const decipher = crypto.createDecipheriv(
    algorithm,
    Buffer.from(key, "hex"),
    Buffer.from(hash.iv, "hex")
  );

  const decrpyted = Buffer.concat([
    decipher.update(Buffer.from(hash.content, "hex")),
    decipher.final(),
  ]);

  return decrpyted.toString();
};

export const getRandomMasterKey = () => {
  const masterKey = crypto.randomBytes(32);
  return masterKey.toString("hex");
};

export const multipleHash = (str, n) => {
  console.log('multipleHash: \n', str, n)
  let hash = crypto.createHash("sha256");

  for (let i = 0; i < n; i++) {
    hash = hash.update(str);
  }

  return hash.digest("hex");
};

export const xorHexStrings = (a, b) => {
  console.log('xorHexStrings: \n', a, b)
  if (!Buffer.isBuffer(a)) a = Buffer.from(a, "hex");
  if (!Buffer.isBuffer(b)) b = Buffer.from(b, "hex");

  let res = [];

  if (a.length > b.length) {
    for (let i = 0; i < b.length; i++) {
      res.push(a[i] ^ b[i]);
    }
  } else {
    for (let i = 0; i < a.length; i++) {
      res.push(a[i] ^ b[i]);
    }
  }

  return Buffer.from(res).toString("hex");
};

// export const hashKeyFile = (level, password = "") => {
//   // let dKeyHash = localStorage.getItem('D_KEY_HASH');
//   let keyFileHash = localStorage.getItem("KEY_FILE_HASH");
//   // let mKeyHash = localStorage.getItem('MASTER_KEY_HASH');

//   let mKeyHash = getCookie("MASTER_KEY_HASH");
//   let dKeyHash = getCookie("D_KEY_HASH");

//   let keyFile = {};

//   if (
//     level === 2 &&
//     dKeyHash &&
//     keyFileHash &&
//     dKeyHash.length &&
//     keyFileHash.length
//   ) {
//     const P = multipleHash(password, 10);

//     const masterKeyHash = xorHexStrings(P, dKeyHash);

//     try {
//       keyFile = JSON.parse(decrypt(JSON.parse(keyFileHash), masterKeyHash));
//     } catch (e) {
//       return {
//         success: false,
//         message: "Password is wrong.",
//       };
//     }
//   } else if (
//     level === 1 &&
//     mKeyHash &&
//     mKeyHash.length &&
//     keyFileHash &&
//     keyFileHash.length
//   ) {
//     try {
//       keyFile = JSON.parse(decrypt(JSON.parse(keyFileHash), mKeyHash));
//     } catch (e) {}
//   } else {
//     const masterKey = getRandomMasterKey();

//     // keyFile = {
//     //   liberdus: {
//     //     asset: {
//     //       asset_name: "Liberdus",
//     //       asset_id: "liberdus",
//     //       asset_path: "/liberdus",
//     //       asset_icon: "https://liberdus.com/img/banner/2.png",
//     //     },
//     //     data: {
//     //       name: "liberdusInfo",
//     //       updatedAt: 0,
//     //     },
//     //   },
//     //   bitcoin: {
//     //     asset: {
//     //       asset_name: "Bitcoin",
//     //       asset_id: "bitcoin",
//     //       asset_path: "/bitcoin",
//     //       asset_icon:
//     //         "https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
//     //     },
//     //     data: {
//     //       name: "bitcoinInfo",
//     //       updatedAt: 0,
//     //     },
//     //   },
//     //   dash: {
//     //     asset: {
//     //       asset_name: "Dash",
//     //       asset_id: "dash",
//     //       asset_path: "/dash",
//     //       asset_icon: "https://media.dash.org/wp-content/uploads/dash-d.png",
//     //     },
//     //     data: {
//     //       name: "dashInfo",
//     //       updatedAt: 0,
//     //     },
//     //   },
//     // };

//     if (level === 1) {
//       // localStorage.setItem('MASTER_KEY_HASH', masterKey);
//       setCookie("MASTER_KEY_HASH", masterKey, "/coinica");
//     } else if (level === 2) {
//       const P = multipleHash(password, 10);
//       const D = xorHexStrings(P, masterKey);
//       // localStorage.setItem('D_KEY_HASH', D);
//       setCookie("D_KEY_HASH", D, "/coinica");
//     }

//     keyFileHash = encrypt(JSON.stringify(keyFile), masterKey);
//     localStorage.setItem("KEY_FILE_HASH", JSON.stringify(keyFileHash));
//   }

//   return {
//     success: true,
//     keyFile,
//   };
// };

export const hashKeyFile = (level, asset = "", password = "") => {
  // let dKeyHash = localStorage.getItem('D_KEY_HASH');
  let keyFileHash = localStorage.getItem("KEY_FILE_HASH");
  // let mKeyHash = localStorage.getItem('MASTER_KEY_HASH');

  let mKeyHash = getCookie("MASTER_KEY_HASH");
  let dKeyHash = getCookie("D_KEY_HASH");
  let keyFile = {};
  if (
    // in case security level is 2
    level === 2 &&
    dKeyHash &&
    keyFileHash &&
    dKeyHash.length &&
    keyFileHash.length
  ) {
    const P = multipleHash(password, 10);
    const masterKeyHash = xorHexStrings(P, dKeyHash);
    try {
      keyFile = JSON.parse(decrypt(JSON.parse(keyFileHash), masterKeyHash));
      if (asset.length > 0) {
        keyFile[asset] = {
          asset: {
            amount: 0,
            value: 0,
            currency: "USD",
            updatedAt: 0,
          },
          data: {},
        };
        keyFileHash = encrypt(JSON.stringify(keyFile), masterKeyHash);
        localStorage.setItem("KEY_FILE_HASH", JSON.stringify(keyFileHash));
      }
    } catch (e) {
      console.log(e)
      return {
        success: false,
        message: "Password is wrong.",
      };
    }
  } else if (
    // in case security level is 1
    level === 1 &&
    mKeyHash &&
    mKeyHash.length &&
    keyFileHash &&
    keyFileHash.length
  ) {
    try {
      keyFile = JSON.parse(decrypt(JSON.parse(keyFileHash), mKeyHash));
      if (asset.length > 0) {
        keyFile[asset] = {
          asset: {
            amount: 0,
            value: 0,
            currency: "USD",
            updatedAt: 0,
          },
          data: {},
        };
      }
      keyFileHash = encrypt(JSON.stringify(keyFile), mKeyHash);
      localStorage.setItem("KEY_FILE_HASH", JSON.stringify(keyFileHash));
    } catch (e) {
      console.log(e)
    }
  } else {
    const masterKey = getRandomMasterKey();

    if (level === 1) {
      // localStorage.setItem('MASTER_KEY_HASH', masterKey);
      setCookie("MASTER_KEY_HASH", masterKey, "/coinica");
    } else if (level === 2) {
      const P = multipleHash(password, 10);
      const D = xorHexStrings(P, masterKey);
      // localStorage.setItem('D_KEY_HASH', D);
      setCookie("D_KEY_HASH", D, "/coinica");
    }

    keyFileHash = encrypt(JSON.stringify(keyFile), masterKey);
    localStorage.setItem("KEY_FILE_HASH", JSON.stringify(keyFileHash));
  }

  return {
    success: true,
    keyFile,
  };
};

export const upgradeKeyFile = (keyFile, password) => {
  const masterKey = getRandomMasterKey();

  const P = multipleHash(password, 10);
  const D = xorHexStrings(P, masterKey);
  // localStorage.setItem('D_KEY_HASH', D);
  setCookie("D_KEY_HASH", D, "/coinica");
  // localStorage.removeItem('MASTER_KEY_HASH');
  deleteCookie("MASTER_KEY_HASH", "/coinica");

  let keyFileHash = encrypt(JSON.stringify(keyFile), masterKey);
  localStorage.setItem("KEY_FILE_HASH", JSON.stringify(keyFileHash));
  localStorage.setItem("SECURITY_LEVEL", "2");

  return {
    success: true,
    keyFile,
  };
};

export const encryptAndStoreKeyFile = (password, keyFile, level) => {
  let keyFileHash, masterKey = '';

  if (level == 2) {
    let dKeyHash = getCookie("D_KEY_HASH");
    const P = multipleHash(password, 10);
    masterKey = xorHexStrings(P, dKeyHash);
  }
  if (level == 1) {
    masterKey = getCookie("MASTER_KEY_HASH");
  }

  console.log('encryptAndStoreKeyFile: \n', keyFile)  

  keyFileHash = encrypt(JSON.stringify(keyFile), masterKey);
  localStorage.setItem("KEY_FILE_HASH", JSON.stringify(keyFileHash));

  return keyFile;
};

// export const updateWalletKeys = (walletKeys, key, password) => {
//   const itemName = `coinica-${walletKeys[key].asset.asset_id}`;
//   let originData = {};
//   const hashKey = getCookie(itemName);

//   try {
//     originData = JSON.parse(
//       decrypt(JSON.parse(localStorage.getItem(itemName)), hashKey)
//     );
//   } catch (e) {
//     console.log(e);
//   }

//   if (
//     originData &&
//     originData.updatedAt &&
//     walletKeys[key].data &&
//     walletKeys[key].data.updatedAt < originData.updatedAt
//   ) {
//     walletKeys[key].data = originData;
//     encryptAndStoreKeyFile(password, walletKeys);
//   }

//   return walletKeys;
// };

export const updateWalletKeys = (walletKeys, key, password = "", level) => {
  if (!password) {
    password = ""
  }

  const itemName = `coinica-${key}`;
  let originData = {};
  const hashKey = getCookie(itemName);
  try {
    originData = JSON.parse(
      decrypt(JSON.parse(localStorage.getItem(itemName)), hashKey)
    );
    console.log('originData: \n', originData, JSON.parse(localStorage.getItem(itemName)))
  } catch (e) {
    console.log(e);
  }

  console.log('walletKeys: \n', walletKeys);
  if (
    originData &&
    walletKeys[key] &&
    walletKeys[key].asset?.updatedAt < originData.asset?.updatedAt
  ) {
    walletKeys[key] = originData;
    encryptAndStoreKeyFile(password, walletKeys, level);
  }
  return walletKeys;
};
