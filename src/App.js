import React, { Component } from 'react';
import { Layout, Result } from 'antd';
import './App.scss';
import WalletHome from './containers/WalletHome';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { LeftMenu, TopHeader, Login, BottomFooter } from './components';
import coinicaLogo from './assets/images/coinica_logo.png';
import { setWalletKeys, setAuth, setPassword, setSecurityLevel, setIsMobile } from './redux/actions';
import { hashKeyFile, getSecurityLevel, getStandaloneInfo } from './utils'

const { Header, Content, Sider } = Layout;
let platform = require('platform');
class App extends Component {
  constructor(props) {
    super(props);

    try {
      const level = getSecurityLevel();

      if (level === 1) {
        let res = hashKeyFile(1);
        props.setWalletKeys(res.keyFile);
        props.setAuth(true);
      }

      props.setSecurityLevel(level);

      let {
        isStandalone,
        useStandaloneMsg,
        shouldUpdateMsg
      } = getStandaloneInfo(platform)

      this.state = {
        collapsed: false,
        isMobile: false,
        useStandaloneMsg,
        isStandalone
      };

      if (shouldUpdateMsg) {
        setTimeout(this.updateUseStandaloneMsg, 3000)
      }

    } catch (e) {}
  }

  updateUseStandaloneMsg = () => {
    this.setState({
      useStandaloneMsg: "Open Coinica from the homescreen"
    })
  }

  onBreakpoint = (broken) => {
    if (broken && !this.props.isMobile) {
      this.setState({
        isMobile: true,
        collapsed: true,
      });
      this.props.setIsMobile(true);
    }
    if (!broken && this.props.isMobile) {
      this.setState({
        isMobile: false,
        collapsed: false,
      });
      this.props.setIsMobile(false);
    }
  }

  render() {
    console.log(
      'app render: \n',
      platform,
      platform.os,
      platform.os.toString()
    );
    // isStandalone = true; // Just for test, Remove after the test!

    const {isMobile} = this.props
    const { isStandalone, useStandaloneMsg } = this.state

    return (
      <div className='App'>
        {isStandalone ? (
          <div>
            {this.props.auth ? (
              <Layout id='components-layout-demo-responsive'>
                <Sider
                  breakpoint='lg'
                  collapsedWidth='0'
                  collapsed={this.props.isMobile}
                  trigger={null}
                  onBreakpoint={this.onBreakpoint}
                  width={250}
                >
                  <div className='logo'>
                    <span className='logo-image'>
                      <img src={coinicaLogo} alt='logo' />
                    </span>
                    <span className='logo-name'>Coinica</span>
                  </div>

                  <div className='left-menu-wrapper'>
                    <LeftMenu />
                  </div>
                </Sider>

                <Layout className='right-layout'>
                  {
                    !isMobile &&
                    <Header
                      className='site-layout-sub-header-background'
                      style={{ padding: 0 }}
                    >
                      <TopHeader />
                    </Header>
                  }

                  <Content style={{ margin: '24px 16px 0' }}>
                    {this.props.upgrading ? (
                      <Login />
                    ) : (
                      <div
                        className='site-layout-background'
                        style={{ padding: 24 }}
                      >
                        <Switch>
                          <Route path='/coinica' component={WalletHome} exact />
                        </Switch>
                      </div>
                    )}
                  </Content>

                  <BottomFooter />
                </Layout>
              </Layout>
            ) : (
              <Layout>
                <Login />
              </Layout>
            )}
          </div>
        ) : (
          <Layout>
            <Result icon={<img src={coinicaLogo} alt="Coinica Logo" style={{width: '100px'}} />} title={useStandaloneMsg} />
          </Layout>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth.auth,
    upgrading: state.auth.upgrading,
    securityLevel: state.auth.securityLevel,
    isMobile: state.common.isMobile,
  };
}

const mapDispatchToProps = (dispatch) => ({
  setWalletKeys(arg) {
    dispatch(setWalletKeys(arg));
  },
  setAuth(arg) {
    dispatch(setAuth(arg));
  },
  setPassword(arg) {
    dispatch(setPassword(arg));
  },
  setSecurityLevel(arg) {
    dispatch(setSecurityLevel(arg));
  },
  setIsMobile(arg) {
    dispatch(setIsMobile(arg));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
