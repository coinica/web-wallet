const SET_AUTH = 'SET_AUTH';
const SET_EMAIL = 'SET_EMAIL';
const SET_PASSWORD = 'SET_PASSWORD';
const SET_UPGRADING = 'SET_UPGRADING';
const SET_SECURITY_LEVEL = 'SET_SECURITY_LEVEL';

const setAuth = (data) => {
  return {
    type: SET_AUTH,
    payload: data,
  };
};

const setPassword = (data) => {
  return {
    type: SET_PASSWORD,
    payload: data,
  };
};

const setEmail = (data) => {
  return {
    type: SET_EMAIL,
    payload: data,
  };
};

const setUpgrading = (data) => {
  return {
    type: SET_UPGRADING,
    payload: data,
  };
};

const setSecurityLevel = (data) => {
  return {
    type: SET_SECURITY_LEVEL,
    payload: data,
  };
};

export {
  SET_AUTH,
  SET_EMAIL,
  SET_PASSWORD,
  SET_UPGRADING,
  SET_SECURITY_LEVEL,
  setAuth,
  setEmail,
  setPassword,
  setUpgrading,
  setSecurityLevel
};
