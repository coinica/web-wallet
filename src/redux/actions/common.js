const SET_IS_MOBILE = 'SET_IS_MOBILE';

const setIsMobile = (payload = true) => {
  return {
    type: SET_IS_MOBILE,
    payload,
  };
};

export {
  SET_IS_MOBILE,
  setIsMobile
};
