import {
  SET_IS_MOBILE
} from '../actions/common';

const initialState = {
  isMobile: false
};

const commonReducer = (previousState = initialState, action) => {
  switch (action.type) {
    case SET_IS_MOBILE:
      return {
        ...previousState,
        isMobile: action.payload,
      };
    default:
      return previousState;
  }
};

export { commonReducer };
