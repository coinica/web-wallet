import {
  SET_AUTH,
  SET_EMAIL,
  SET_PASSWORD,
  SET_UPGRADING,
  SET_SECURITY_LEVEL,
} from '../actions/auth';

const initialState = {
  auth: false,
  email: null,
  password: null,
  upgrading: false,
  securityLevel: 1,
};

const authReducer = (previousState = initialState, action) => {
  switch (action.type) {
    case SET_AUTH:
      return {
        ...previousState,
        auth: action.payload,
      };
    case SET_PASSWORD:
      return {
        ...previousState,
        password: action.payload,
      };
    case SET_EMAIL:
      return {
        ...previousState,
        email: action.payload,
      };
    case SET_UPGRADING:
      return {
        ...previousState,
        upgrading: action.payload,
      };
    case SET_SECURITY_LEVEL:
      return {
        ...previousState,
        securityLevel: action.payload,
      };
    default:
      return previousState;
  }
};

export { authReducer };
