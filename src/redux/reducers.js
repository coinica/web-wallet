import { combineReducers } from 'redux';
import { authReducer } from './reducers/auth';
import { walletReducer } from './reducers/wallet';
import { commonReducer } from './reducers/common';

export default combineReducers({
  auth: authReducer,
  wallet: walletReducer,
  common: commonReducer
})